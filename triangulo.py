import sys


def line(number):
    if number < 1 or number > 9:
        raise ValueError("Number out of range (1-9)")
    return str(number) * number


def triangle(number):
    if number < 1 or number > 9:
        raise ValueError("Number out of range (1-9)")
    result = ""
    for i in range(1, number + 1):
        result += line(i) + '\n'
    return result


def main():
    if len(sys.argv) != 2:
        print("Please provide a number (between 1 and 9) as argument")
        sys.exit(1)

    try:
        num = int(sys.argv[1])
        print(triangle(num))
    except ValueError:
        print("Please provide a valid number (between 1 and 9) as argument")


if __name__ == "__main__":
    main()
